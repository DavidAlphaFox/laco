; rational-division
(define (div x y)
    (/ x y))
(display (div 31999 32000))
(newline)
(display (div 31999 -32000))
(newline)
(display (div -31999 32000))
(newline)
(display (div -31999 -32000))
(newline)
(display (div 32768 4096))
(newline)
(display (div -32768 4096))
(newline)
(display (div 32768 -4096))
(newline)
(display (div -32768 -4096))
(newline)
(display (div 65535 65534))
(newline)
(display (div -65535 65534))
(newline)
(display (div 65535 -65534))
(newline)
(display (div -65535 -65534))
(newline)
(display (div 3/10 2))
(newline)
(display (div -3/10 2))
(newline)
(display (div 3/10 -2))
(newline)
(display (div -3/10 -2))
(newline)
(display (div 2 3/10))
(newline)
(display (div -2 3/10))
(newline)
(display (div 2 -3/10))
(newline)
(display (div -2 -3/10))
(newline)
(display (div 6553 1/10))
(newline)
(display (div -6553 1/10))
(newline)
(display (div 6553 -1/10))
(newline)
(display (div -6553 -1/10))
(newline)
(display (div 7/100 5/3))
(newline)
(display (div -7/100 5/3))
(newline)
(display (div 7/100 -5/3))
(newline)
(display (div -7/100 -5/3))
(newline)
